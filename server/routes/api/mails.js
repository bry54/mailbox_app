const express = require('express');
const router = express.Router();
const { readMail, sendMail, fetchMails } = require('../../controllers/MailController');

router.route('/').post(fetchMails);

router.route('/read').post(readMail);

router.route('/send').post(sendMail);

module.exports = router;
