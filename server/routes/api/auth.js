const express = require('express');
const router = express.Router();
const { signUpUser, fetchUsers, signOutUser, signInUser, reloadUser } = require('../../controllers/AuthController');

router.route('/signup').post(signUpUser);

router.route('/fetch').get(fetchUsers);

router.route('/signin').post(signInUser);

router.route('/signout').post(signOutUser);

router.route('/reload').post(reloadUser);

module.exports = router;
