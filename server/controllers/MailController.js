const User = require('../models/User');
const Mail = require('../models/Mail');

/*
@desc    Get user's specific mail, and mark as read
@route   POST /api/mails/read
@access  Public
 */
exports.readMail = async (req, res, next) => {
    const {mailId} = req.body;
    try {
        const mail = await Mail.findOne({_id: mailId });
        mail.isRead = true;
        mail.readAt = Date.now();
        await mail.save();

        await User.updateOne(
            {_id: mail.sentTo.id, 'receivedEmails._id': mail._id},
            { $set: {
                    "receivedEmails.$.isRead" : mail.isRead,
                    "receivedEmails.$.readAt" : mail.readAt,
                }
            }
        )

        await User.updateOne(
            {_id: mail.sentFrom.id, 'sentEmails._id': mail._id},
            { $set: {
                    "sentEmails.$.isRead" : mail.isRead,
                    "sentEmails.$.readAt" : mail.readAt,
                }
            }
        )

        return res.status(200).json({
            success: true,
            mail: mail
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
}

/*
@desc    Get emails belonging to a user
@route   POST /api/mails/
@access  Public
 */
exports.fetchMails = async (req, res, next) => {
    const {userId} = req.body;
    try {
        const user = await User.findOne({
            _id: userId,
        });

        return res.status(200).json({
            success: true,
            mails: {
                sent: user.sentEmails,
                received: user.receivedEmails
            }
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: err.response
        });
    }
}

/*
@desc    Send mail to another person
@route   POST /api/mails/send
@access  Public
 */
exports.sendMail = async (req, res, next) => {
    const {sender, receiver, message} = req.body;

    try {
        const receiverObj = await User.findOne({_id: receiver});
        const senderObj = await User.findOne({_id: sender});

        const mail = await Mail.create({
            subject: message.subject,
            content: message.content,
            sentTo: {
                id: receiverObj._id,
                fullName: receiverObj.fullName
            },
            sentFrom: {
                id: senderObj._id,
                fullName: senderObj.fullName
            },
        });

        receiverObj.receivedEmails.push(mail);
        receiverObj.save();

        senderObj.sentEmails.push(mail);
        senderObj.save();

        return res.status(200).json({
            success: true,
            mail: mail
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
}
