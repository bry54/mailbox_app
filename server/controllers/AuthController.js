const User = require('../models/User');

/*
@desc    Add user
@route   POST /api/auth/users/create
@access  Public
 */
exports.signUpUser = async (req, res, next) => {
    try {
        const { fullname, username, password } = req.body;

        const user = await User.create({
            fullName: fullname,
            userName: username,
            password: password,
        });

        return res.status(201).json({
            success: true,
            data: user
        });
    } catch (err) {
        if(err.name === 'ValidationError') {
            const messages = Object.values(err.errors).map(val => val.message);

            return res.status(400).json({
                success: false,
                error: messages
            });
        } else {
            return res.status(500).json({
                success: false,
                error: 'Server Error'
            });
        }
    }
}

/*
@desc    Get all users
@route   GET /api/auth/users/fetch
@access  Public
*/
exports.fetchUsers = async (req, res, next) => {
    try {
        const users = await User.find();

        return res.status(200).json({
            success: true,
            users: users
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
}

/*
@desc    Login user
@route   POST /api/auth/users/login
@access  Public
 */
exports.signInUser = async (req, res, next) => {
    const {username, password} = req.body;
    try {
        const user = await User.findOne({
            userName: username,
            password: password
        });

        return res.status(200).json({
            success: true,
            user: user
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
}

/*
@desc    Reload user
@route   POST /api/auth/users/reload
@access  Public
 */
exports.reloadUser = async (req, res, next) => {
    const {userId} = req.body;
    try {
        const user = await User.findById(userId);

        return res.status(200).json({
            success: true,
            user: user
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
}

/*
@desc    Logout user
@route   POST /api/auth/users/logout
@access  Public
 */
exports.signOutUser = async (req, res, next) => {
    try {
        const user = await User.find();

        return res.status(200).json({
            success: true,
            count: user.length,
            data: user
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
}
