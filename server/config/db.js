const mongoose = require('mongoose');

const dbConnection = async () => {
    const dbConnectionURL = JSON.parse(process.env.USE_LOCAL_MONGO) ? process.env.MONGO_LOCAL_URI : process.env.MONGO_REMOTE_URI
    await mongoose.connect(dbConnectionURL, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true  })
        .then(() => console.log('Connected to mongo'))
        .catch((error) => {
            console.log(error);
            process.exit(1)
        })
}

module.exports = dbConnection
