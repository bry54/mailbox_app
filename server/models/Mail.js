const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MailSchema = new Schema({
    isRead: { type: Boolean, default: false },
    subject: { type: String, required: true },
    content: { type: String, required: true },
    sentTo: { type: Object, required: true },
    sentFrom: { type: Object, required: true },
    readAt: { type: Date, required: false, default: null },
    deliveredAt: { type: Date, required: true, default: Date.now() }
});

module.exports = Mail = mongoose.model('mail', MailSchema);