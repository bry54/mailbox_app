const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    userName: { type: String, required: false, unique: true },
    fullName: { type: String, required: true },
    password: { type: String, required: true },
    sentEmails: { type: Array, default: new Array() },
    receivedEmails: { type: Array, default: new Array() },
    createdAt: { type: Date, default: new Date() },
});

module.exports = User = mongoose.model('user', UserSchema);
