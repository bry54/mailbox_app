const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dbConnection = require('./config/db')
const dotenv = require('dotenv')

dotenv.config({ path: './config/config.env'});

const app = express();

app.use(bodyParser.json())
app.use(cors())
app.options('*', cors())

dbConnection();

const mailRoutes = require('./routes/api/mails');
const authRoutes = require('./routes/api/auth');

app.use('/api/auth/users', authRoutes);
app.use('/api/mails', mailRoutes);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Running on in ${process.env.NODE_ENV} mode on port ${port}`));
