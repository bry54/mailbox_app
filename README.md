# Fullstack Mail Application
Homework assignment for Fullstack Job position at MBL HighTech Ltd. Its a mock up for a mailbox application. 

## Client Screenshots
![Auth Screen](./samples/mbl_auth.png)

![Home Screen](./samples/mbl_home.png)

![Inbox Screen](./samples/mbl_inbox.png)

![Compose Screen](./samples/mbl_compose.png)

![Sent Screen](./samples/mbl_sent.png)


## Setting up the application environment
- Clone the application in terminal window `git clone https://bry54@bitbucket.org/bry54/mailbox_app.git`  
- Change your working directory to the cloned repository `cd mailbox_app`  
  
## Easy Installation  
- ***YOU WILL NEED TO HAVE [Yarn package manager](https://yarnpkg.com/) install on your computer.***  
- On terminal tested on (MacOS/Linux) but should work on windows terminal too.  
`yarn && yarn --cwd server && yarn --cwd client`  
`yarn dev` 

## Manual Installation
### Server Setup/Configuration 
In the project directory `mailbox_app`, run the following terminal commands to start server:
#### `cd server`
#### `yarn` or `npm install`
#### `yarn dev` or `npm run dev`

#### API Endpoints
- `[POST] http://localhost:5000/api/auth/users/signup` Creating users
- `[GET] http://localhost:5000/api/auth/users/fetch` Fetch users in database
- `[POST] http://localhost:5000/api/mails/send` Sending Mail
- `[POST] http://localhost:5000/api/mails/read` Toggle mail read status

#### API Interaction
- React application is fully setup to communication with server for mail functions.
- Alternatively user POSTMAN to communicate with server. To setup postman for use please import the attached [POSTMAN Collection](./samples/Mail%20App%20MERN.postman_collection.json) into your postman application.
- I have connected to my mongodb remote host, please make sure your proxy does not block remote mongodb  connections,
if remote mongo is blocked by your proxy or you want to use your own database connections please edit [Database Configurations](./server/config/config.env) and update the following:  
### `USE_LOCAL_MONGO = true`  
### `MONGO_LOCAL_URI = mongodb://MONGO_DB_USER_HERE:MONGO_DB_USER_PASSWORD_HERE@127.0.0.1:27017/mbl_hightech?authSource=admin`.  
### `MONGO_REMOTE_URI = mongodb+srv://MONGO_DB_USER_HERE:MONGO_DB_USER_PASSWORD_HERE@cluster0-ecve2.gcp.mongodb.net/mbl_hightech?retryWrites=true&w=majority`.  

- Remote Database already has users, if you choose to use local database connection or your own db configurations please create some users using POSTMAN client.

### Client ReactJS App Setup/Configuration 
In another terminal window, run the following commands to start the client application (Assuming you are in server directory)  
 
#### `cd ../client`  
#### `yarn` or `npm install`
#### `yarn start` or `npm run start`

### Running the application in development mode.
- Server should be running on [http://localhost:5000](http://localhost:5000)  

- Open [http://localhost:3000](http://localhost:3000) to view the client application in browser.

## Technology stack used:
- [Mongodb](http://mongodb.com)
- [Express](https://expressjs.com/)
- [NodeJS](https://nodejs.org/docs/latest-v12.x/api/)
- [ReactJs](https://reactjs.org)
- [React Router](https://reactrouter.com/web)
- [Bulma CSS Framework](https://bulma.io/)
- [Axios](https://github.com/axios/axios)
- [Yarn](https://yarnpkg.com/) or [npm](https://www.npmjs.com/get-npm)
