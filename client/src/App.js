import Header from "./components/Header";
import Footer from "./components/Footer";
import ContentArea from "./components/ContentArea";
import {BrowserRouter as Router} from "react-router-dom";
import {GlobalProvider} from "./context/GlobalState";

function App() {
    return (
        <GlobalProvider>
            <div className="App">
                <Router>
                    <Header />
                    <ContentArea />
                    <Footer/>
                </Router>
            </div>
        </GlobalProvider>
    );
}

export default App;
