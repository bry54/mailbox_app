import React from 'react'
import {useLocation} from "react-router-dom";

export default () => {
    const {message, type} = useLocation();

    return (
        <React.Fragment>
            <article className="message is-dark">
                <div className="message-body">
                    {
                        type === 'SENT' ?
                            <span><strong>TO: </strong> {message.sentTo.fullName} <br/><br/></span>
                            :
                            <span><strong>FROM: </strong> {message.sentFrom.fullName} <br/><br/></span>
                    }
                    <span><strong>SUBJECT: </strong> {message.subject}</span>
                    <p>
                        <hr/>
                        {message.content}
                    </p>
                </div>
            </article>
        </React.Fragment>
    )
}
