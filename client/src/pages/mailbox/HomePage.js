import React, {useContext} from "react";
import {GlobalContext} from "../../context/GlobalState";

export default () => {
    const {user } = useContext(GlobalContext)
    const unread = user.receivedEmails.filter(m => !m.isRead)

    return (
        <React.Fragment>
            <div className="tile is-parent">
                <div className="tile is-child box">
                    <p className="is-size-4">Hie {user.fullName},</p><br/>
                    <p>
                        Hello, page you are viewing is the home-page for your mail account. Below you can find usage statistics for your mailbox.
                    </p>
                    <hr/>
                    <nav className="level">

                        <div className="level-item has-text-centered">
                            <div>
                                <p className="heading">Unread Messages</p>
                                <p className="title">{unread.length}</p>
                            </div>
                        </div>
                        <div className="level-item has-text-centered">
                            <div>
                                <p className="heading">Total Messages</p>
                                <p className="title">{user.receivedEmails.length + user.sentEmails.length}</p>
                            </div>
                        </div>
                        <div className="level-item has-text-centered">
                            <div>
                                <p className="heading">Received Messages</p>
                                <p className="title">{user.receivedEmails.length}</p>
                            </div>
                        </div>
                        <div className="level-item has-text-centered">
                            <div>
                                <p className="heading">Sent Messages</p>
                                <p className="title">{user.sentEmails.length}</p>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </React.Fragment>
    )
}
