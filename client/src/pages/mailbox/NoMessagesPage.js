export default ({user}) => {
    return (
        <div className="tile is-parent">
            <div className="tile is-child box">
                <p className="is-size-4">Hie {user.fullName},</p>
                <p>
                    There are no message to view in this mailbox folder.
                </p>
            </div>
        </div>
    )
}
