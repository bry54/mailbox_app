import React, {useContext, useState} from "react";
import {GlobalContext} from "../../context/GlobalState";
import {Link} from "react-router-dom";

export default () => {
    const {user, users, sendMail} = useContext(GlobalContext)

    const [message, setMessageInputs] = useState({
        to: '',
        subject: '',
        content: ''
    });

    const handleMessageInputs = (value, key) =>{
        setMessageInputs({
            ...message,
            [key] : value
        })
    };

    return (
        <>
            <div className="field">
                <label className="label">
                    To <small>For test purposes click on the user to send them a message</small>
                </label>
                <div className="control">
                    {users.map((user, index) =>(
                        <button className={`button is-small ${message.to === user._id ? 'is-success' : 'is-warning is-light'}`}
                                key={index.toString()}
                                onClick={() => handleMessageInputs(user._id, 'to')}>
                            {user.userName}
                        </button>
                    ))}
                </div>

            </div>

            <div className="field">
                <label className="label">Subject</label>
                <div className="control">
                    <input
                        value={message.subject}
                        onChange={(event) => handleMessageInputs(event.target.value, 'subject')}
                        className="input is-small" type="text" placeholder="Subject" />
                </div>
            </div>

            <div className="field">
                <label className="label">Message</label>
                <div className="control">
                    <textarea
                        value={message.content}
                        onChange={(event) => handleMessageInputs(event.target.value, 'content')}
                        className="textarea is-small" placeholder="Type message here"></textarea>
                </div>
            </div>

            <div className="field is-grouped is-right">
                <div className="control">
                    <button
                        onClick={ () => sendMail(message) }
                        className="button is-link is-small">
                        Send
                    </button>
                </div>
                <div className="control">
                    <Link
                        to={{
                            pathname: '/',
                        }}>
                        <button className="button is-link is-light is-small">
                            Cancel
                        </button>
                    </Link>
                </div>
            </div>
        </>
    )
}
