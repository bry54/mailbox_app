import React, {useContext} from "react";
import {GlobalContext} from "../../context/GlobalState";
import {useLocation} from "react-router-dom";
import InboxMessage from "../../components/InboxMessage";
import SentMessage from "../../components/SentMessage";
import NoMessagesPage from "./NoMessagesPage";

export default () => {
    const {type} = useLocation();
    const {user, readMail} = useContext(GlobalContext)

    let MessageComponent = InboxMessage;
    let messages = user.receivedEmails;

    if (type === 'SENT'){
        MessageComponent = SentMessage;
        messages = user.sentEmails;
    } else if(type === 'INBOX'){
        MessageComponent = InboxMessage;
        messages = user.receivedEmails;
    }

    if (!messages || messages.length<1)
        return <NoMessagesPage user={user}/>
    else
        return (
            <div>
                {messages.map((message, index) => (
                    <MessageComponent
                        key={index.toString()}
                        message={message}
                        readMail={readMail}/>
                ))}
            </div>
        )
}
