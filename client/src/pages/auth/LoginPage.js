import React, {useContext, useEffect} from "react";
import {GlobalContext} from "../../context/GlobalState";
import UserCard from "../../components/UserCard";

export default () => {
    const {users, fetchUsers, setUser } = useContext(GlobalContext);

    useEffect(() =>{
        fetchUsers()
    }, [])

    return (
        <React.Fragment>
            <div className="container is-mobile">
                <div className="message is-info">
                    <p className="message-header">Sample Users</p>
                    <p className="message-body is-centered">
                        This is a mockup web application to emulate mailbox functions. No strict authentication is implemented, select user to proceed with. You can send and receive messages as one of the users defined below
                    </p>
                    <div className="columns is-mobile" style={{padding: '30px 15px', marginBottom: '20px'}}>
                        {users.map((user, index) =>(
                            <UserCard key={index.toString()} user={user} onUserClick={setUser}/>
                        ))}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
