export default ()=>(
    <footer className="footer" style={{position:'fixed', bottom:'0', width: '100%'}}>
        <div className="content has-text-centered">
            <p>
                <strong>MBL Homework Task</strong> by <a target='_blank' href="https://www.iambriansith.com">Brian Paidamoyo Sithole</a>.
            </p>
        </div>
    </footer>
)
