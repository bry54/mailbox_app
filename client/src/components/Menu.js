import {Link, useLocation} from "react-router-dom";
import {useContext} from "react";
import {GlobalContext} from "../context/GlobalState";

const menuItems = [{
    title: 'Your Mailbox',
    items: [
        {title: 'Home', destination: '/', action: null},
        {title: 'Compose', destination: '/compose', action: null},
        {title: 'Inbox', destination: '/inbox', type: 'INBOX', action: null},
        {title: 'Sent', destination: '/sent', type: 'SENT', action: null},
    ]
}, {
    title: 'Information',
    items: [
        {title: 'Refresh', destination: '/', action: 'REFRESH'},
        {title: 'Logout', destination: '/', action: 'LOGOUT'},
    ]
}];

export default () => {
    const { refreshData, setUser } = useContext(GlobalContext);

    const actions = {
        REFRESH: refreshData,
        LOGOUT: setUser
    }

    return (
        <aside className="menu">
            {menuItems.map((menuItem, index) => (
                <div key={index.toString()}
                     style={{marginBottom: '5px'}}>
                    <p className="menu-label"> {menuItem.title} </p>
                    <ul className="menu-list">
                        {menuItem.items.map((item, index) => {
                            return (
                                <Link
                                    onClick={item.action ? ()=>actions[item.action](null) : ()=>{}}
                                    className={(!item.action && useLocation().pathname === item.destination) ? 'is-active' : ''}
                                    key={index.toString()}
                                    to={{
                                        pathname: item.destination,
                                        type: item.type,
                                    }}>
                                    <li>
                                        <span>{item.title}</span>
                                    </li>
                                </Link>
                            )
                        })}
                    </ul>
                </div>
            ))}
        </aside>
    )
}
