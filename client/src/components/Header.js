import React, {useContext} from "react";
import {GlobalContext} from "../context/GlobalState";
import {Link} from "react-router-dom";

export default () => {
    const {user, refreshData, setUser } = useContext(GlobalContext)

    return (
        <div>
            <nav className="navbar" role="navigation" aria-label="main navigation">
                <div className="navbar-brand">
                    <a role="button" className="navbar-burger" aria-label="menu" aria-expanded="false"
                       data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div id="navbarBasicExample" className="navbar-menu">
                    <div className="navbar-start">
                        <Link to='/'>
                            <span className="navbar-item is-size-4">
                                MBL Mailbox
                            </span>
                        </Link>
                    </div>

                    {user ? <div className="navbar-end">
                        <a className="navbar-item">
                            {user.fullName}
                        </a>
                        <div className="navbar-item">
                            <div className="buttons">
                                <a className="button is-white" onClick={() => setUser()}>
                                    Log Out
                                </a>
                            </div>
                        </div>
                    </div> : null}
                </div>
            </nav>
            <hr style={{marginTop: '0px'}}/>
        </div>
    )
}
