import Menu from "./Menu";
import AppNavigation from "../navigation/AppNavigation";
import {useContext} from "react";
import {GlobalContext} from "../context/GlobalState";
import LoginPage from "../pages/auth/LoginPage";

export default () => {
    const { user } = useContext(GlobalContext)

    console.log(user)

    if (user)
        return (
            <div className="container">
                <div className="columns">
                    <div className="column is-one-quarter">
                        <Menu/>
                    </div>
                    <div className="column is-three-quarters">
                        <AppNavigation/>
                    </div>
                </div>
            </div>
        )
    else
        return (
            <LoginPage />
        )
}
