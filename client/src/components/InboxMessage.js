import moment from 'moment';
import {Link} from "react-router-dom";

const MAX_LENGTH = 100;

export default ({message, readMail}) => (
    <article className="media">
        <div className="media-left">
            { message.isRead ?
                <strong><span className="tag is-primary is-light">Read</span></strong>
                :
                <strong><span className="tag is-info is-light">Unread</span></strong>
            }
        </div>
        <div className="media-content">
            <div className="content">
                <p>
                    <small>From: {message.sentFrom.fullName}</small> <br/>
                    <small>Subject: {message.subject}</small> <br/>
                    <small style={{maxLines: 1, textOverflow: 'ellipsis'}}>
                        {`${message.content.substring(0, MAX_LENGTH)} ...`}
                    </small>
                </p>
            </div>
            <nav className="level is-mobile">
                <div className="level-left">
                    <div className="tags has-addons">
                        <span className="tag is-dark">Received</span>
                        <span className="tag is-info">{moment(message.deliveredAt).format('LLL')}</span>
                    </div>
                </div>

                <div className="level-right">
                    <span className="level-item">
                        <Link
                            onClick={() =>readMail(message)}
                            to={{
                                pathname: '/mail-detail',
                                message: message,
                                type: 'RECEIVED'
                            }}>
                          <button className="button is-link is-light is-small">Read More</button>
                        </Link>
                    </span>
                </div>
            </nav>
        </div>
    </article>
)
