import moment from 'moment';
import {Link} from "react-router-dom";

const MAX_LENGTH = 100;

export default ({message}) => (
    <article className="media">
        <div className="media-content">
            <div className="content">
                <p>
                    <small>To: {message.sentTo.fullName}</small> <br/>
                    <small>Subject: {message.subject}</small> <br/>
                    <small style={{maxLines: 1, textOverflow: 'ellipsis'}}>
                        {`${message.content.substring(0, MAX_LENGTH)} ...`}
                    </small>
                </p>
            </div>
            <nav className="level is-mobile">
                <div className="level-left">
                    <span className="level-item">
                    <div className="tags has-addons">
                        <span className="tag is-dark">Sent</span>
                        <span className="tag is-success">{moment(message.deliveredAt).format('LLL')}</span>
                    </div>
                    </span>

                    <span className="level-item">
                    <div className="tags has-addons">
                        <span className="tag is-dark">Read</span>
                        {
                            message.isRead ?
                                <span className="tag is-warning">{moment(message.readAt).format('LLL')}</span>
                                :
                                <span className="tag is-warning">Not Read</span>
                        }
                    </div>
                    </span>
                </div>

                <div className="level-right">
                    <span className="level-item">
                        <Link to={{
                            pathname: '/mail-detail',
                            message: message,
                            type: 'SENT'
                        }}>
                          <button className="button is-link is-light is-small">Read More</button>
                        </Link>
                    </span>
                </div>
            </nav>
        </div>
    </article>
)
