import moment from "moment";

export default ({user, onUserClick}) => {
    return (
        <div className="column" onClick={() => onUserClick(user)}>
            <div className="card">
                <div className="card-content">
                    <a className="media">
                        <div className="media-left">
                            <figure className="image is-48x48">
                                <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image"/>
                            </figure>
                        </div>
                        <div className="media-content">
                            <p className="title is-5">{user.fullName}</p>
                            <p className="subtitle is-6">{user.userName}</p>
                        </div>
                    </a>

                    <div className="content">
                        <p className='is-bold'>Sample user</p>
                        <br/>
                        <p className="is-small">Joined {moment(user.createdAt).fromNow()}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
