export default (state, action) => {
    switch (action.type){
        case 'READ_MAIL': {
            return {
                ...state
            }
        }

        case 'FETCH_USERS': {
            return {
                ...state,
                users: action.payload
            }
        }

        case 'SET_USER': {
            return {
                ...state,
                user: action.payload
            }
        }

        case 'SEND_MAIL': {
            return {
                ...state
            }
        }

        case 'REFRESH': {
            return {
                ...state,
                user: action.payload.user
            }
        }

        default: {
            return state
        }
    }
}
