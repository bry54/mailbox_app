import React, {createContext, useReducer} from 'react'
import AppReducer from './AppReducer'
import axios from "axios";
import Swal from 'sweetalert2'

const initialState = {
    users: [],
    user: null
}

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({children}) => {
    const [state, dispatch] = useReducer(AppReducer, initialState);

    const refreshData = async () => {
        await axios.post('http://localhost:5000/api/auth/users/reload',{
            userId: state.user._id
        },{
            headers: {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }).then(res => {
            dispatch({
                type: 'REFRESH',
                payload: res.data
            })
        }).catch(err => {

        })
    }

    const readMail = async (mail) =>{
        await axios.post('http://localhost:5000/api/mails/read', {
            mailId: mail._id
        }, {
            headers: {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }).then(res => {
            dispatch(refreshData())
        }).catch(err => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong! Try again',
            })
        })
    };

    const sendMail = async (mail) => {
        await axios.post('http://localhost:5000/api/mails/send', {
            receiver: mail.to,
            sender: state.user._id,
            message: {
                subject:  mail.subject,
                content: mail.content,
            }
        }, {
            headers: {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }).then(res => {
            dispatch(refreshData())
            Swal.fire({
                icon: 'success',
                title: 'Sent',
                text: 'Your message has been sent',
            })
        }).catch(err => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong! Try again',
            })
        })
    };

    const fetchUsers = async () => {
        await axios.get(
            'http://localhost:5000/api/auth/users/fetch'
        ).then(res =>{
            dispatch({
                type: 'FETCH_USERS',
                payload: res.data.users
            })
        }).catch(err => {

        })
    }

    const setUser = async (user = null) => {
        console.log({setUser: user})
        dispatch({
            type: 'SET_USER',
            payload: user
        })
    }

    return (
        <GlobalContext.Provider value={{
            user: state.user,
            users: state.users,
            readMail: readMail,
            sendMail: sendMail,
            refreshData: refreshData,
            fetchUsers: fetchUsers,
            setUser: setUser
        }}>
            {children}
        </GlobalContext.Provider>
    )
}
