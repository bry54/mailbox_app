import {Route} from "react-router-dom";
import HomePage from "../pages/mailbox/HomePage";
import MailBoxPage from "../pages/mailbox/MailBoxPage";
import MailViewPage from "../pages/mailbox/MailViewPage";
import MailComposer from "../pages/mailbox/MailComposer";

export default () => (
    <>
        <Route exact path={'/'} component={HomePage}/>
        <Route exact path={'/inbox'} component={MailBoxPage}/>
        <Route exact path={'/sent'} component={MailBoxPage}/>
        <Route exact path={'/compose'} component={MailComposer}/>
        <Route exact path={'/mail-detail'} component={MailViewPage}/>
    </>
)
